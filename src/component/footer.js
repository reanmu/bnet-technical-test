import React from 'react'
import './css/footer.css'

let Footer = () => {
    return (
        <div className="footer">
            <p>copyright &copy; by Restiani Andhita Mustikasari</p>
        </div>
    )
}

export default Footer;