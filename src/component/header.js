import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Main from './page/main';
import MainCat from './page/main-cat';
import Category from './category';
import Random from './page/random';
import New from './page/new';
import Detail from './page/detail';
import './css/header.css'

let Header = () => {
    return (
        <Router>
            <div className="navigation">
                <nav className="navbar navbar-expand-lg sticky-top navbar-dark bg-dark">
                    <div className="container-fluid">
                        <a className="navbar-brand"> <Link className="navbar-brand" aria-current="page" to="/">Your Meal</Link></a>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav d-flex mb-3 mb-lg-0">
                                <li className="nav-item p-2 bd-highlight">
                                    <Link className="nav-link" aria-current="page" to="/">Home</Link>
                                </li>
                                <li className="nav-item p-2 bd-highlight">
                                    <Link className="nav-link" to="/category">Category</Link>
                                </li>
                                <li className="nav-item ms-auto p-2 bd-highlight">
                                    <Link className="nav-link" to="/random">Random Recipe Here !</Link>
                                </li>
                                <li className="nav-item ms-auto p-2 bd-highlight">
                                    <Link className="random" to="/new">New Recipe !</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <Switch>
                    <Route exact path="/">
                        <Main />
                    </Route>
                    <Route path="/finding/:search">
                        <Main />
                    </Route>
                    <Route path="/cat/:catId">
                        <MainCat />
                    </Route>
                    <Route path="/category">
                        <Category />
                    </Route>
                    <Route path="/detail/:id">
                        <Detail />
                    </Route>
                    <Route path="/random">
                        <Random />
                    </Route>
                    <Route path="/new">
                        <New />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

export default Header;