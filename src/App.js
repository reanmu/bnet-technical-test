import './component/css/bootstrap.min.css';
import Headers from './component/header';
import Footer from './component/footer';

function App() {
  return (
    <div>
      <Headers />
      <Footer />
    </div>
  );
}

export default App;
